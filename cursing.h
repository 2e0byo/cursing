#ifndef CURSING_H
#define CURSING_H
#include "mbed.h"
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
/* TODO:
   SORT OUT DUPLICATION...
*/

//DigitalOut led3(LED3); // on-board LED3 testing
//0x1B is escape

/** A simple class for sending VT100 terminal escape codes for handling things such as moving the cursor and clearing the screen
 */
class Cursing
{
public:
  /* The following is forked from TermControl */
  Cursing()
  {

  }
  /** @Param term The stream to send the escape codes to
   */
  Cursing(Stream* term)
  {
    Term=term;
  }
  void SetTerminal(Stream* term)
  {
    Term=term;
  }
  Stream* GetTerminal()
  {
    return Term;
  }
  //terminal control
  /** Resets the terminal to defaults
   */
  void Reset()
  {
    Print("\x1B");
    Print("c");
  }
  /** Clears the screen
   */
  void Clear()
  {
    Term->printf("\x1B[2J");
  }
  /** Prints the specified string at coordinates x,y
   *  The cursor position is not changed
   *
   *  @param x The X coordinate, or column
   *  @param y The Y coordinate, or row
   *  @param s The string to print
   */
  void PrintAt(int x, int y, string s)
  {
    SaveCursor();
    ResetCursor();
    SetCursor(x,y);
    Print(s);
    RestoreCursor();
  }
  //todo void PrintfAt(int x, int y, string f, ...);
  /** Get a character from the terminal stream
   */
  char GetChar()
  {
    return Term->getc();
  }
  /** Sets the cursor's position to x,y
   */
  void SetCursor(int x, int y)
  {
    Term->printf("\x1B[%i;%iH",y,x);
  }
  /** Resets the cursor to (0,0), the top left hand corner of the screen
   */
  void ResetCursor()
  {
    Term->printf("\x1B[H");
  }
  /** Saves the cursor on the display-side. It can only remember one cursor position at a time.
   * ie, this can't be nested
   */
  void SaveCursor()
  {
    Term->printf("\x1B[s");
  }
  /** Restores the cursor on the display-side. It can only remember and store one cursor position at a time.
   * ie, this can't be nested
   */
  void RestoreCursor()
  {
    Term->printf("\x1B[u");
  }
  /** Enables a scrolling "window" at the specified beginning and ending rows
   * @param begin The beginning row
   * @param end The ending row
   */
  void EnableScrolling(int begin, int end) //begin and end are row numbers
  {
    Term->printf("\x1B[%i;%ir",begin,end);
  }
  /** Scrolls down the scrolling "window" by 1 line. Note, EnableScrolling is required before this can be called
   */
  void ScrollDown()
  {
    Term->printf("\x1B");
    Term->printf("D");
  }
  /** Alternative method as putty doesn't like the previous
   */
  void AltScrollDown()
  {
    Term->printf("\x1B[B");
  }
  
  /** Scrolls up the scrolling "window" by 1 line. Note, EnableScrolling is required before this can be called
   */
  void ScrollUp()
  {
    Term->printf("\x1BM");
  }
  /** Erases the current line the cursor is on.
   */
  void EraseLine()
  {
    Term->printf("\x1B[2K");
  }
  /** Prints the specified string to the string at the current cursor position
   */
  void Print(string s)
  {
    Term->puts(s.c_str());
  }
  void LocalEchoEnable()
  {
    Term->printf("\x1B[12l");
  }
    void LocalEchoDisable()
  {
    Term->printf("\x1B[12h");
  }

  /* Cursing begins here */

  /** Swap Foreground/Background Colour
   */
  void PrintInvert(string str)
  {
    Term->printf("\x1B[7m%s\x1B[27m",str.c_str());
  }
  /** print specified string *inverted* at (x,y)
   */
  void InvertPrintAt(int x, int y, string s)
  {
    SaveCursor();
    ResetCursor();
    SetCursor(x,y);
    Term->printf("\x1B[7m%s\x1B[27m",s.c_str());
    RestoreCursor();
  }
  /** Draw horizontal line of '-' at (x,y) length l
   */
  void drawline (int x, int y, int l) {
    int i (0);
    for (i = x; i <= l+x; i++) {
      PrintAt(i,y,"-");
    }
  }
  /** Draw vertical line of '|' at (x,y) length l
   */
  void vdrawline (int x, int y, int l) {
    int i (0);
    for (i = y; i <= l+y; i++) {
      PrintAt(x,i,"|");
    }
  }

  void drawbox(int x,int y,int w,int h) {
    int i (0);
    // Corners
    PrintAt(x,y,"+");
    PrintAt(x+w,y,"+");
    PrintAt(x,y+h,"+");
    PrintAt(x+w,y+h,"+");
    // Top Line
    for (i = x+1; i < w+x; i++) {
      PrintAt(i,y,"-");
    }
    // Bottom Line
    i=0;
    for (i = x+1; i < w+x; i++) {
      PrintAt(i,y+h,"-");
    }
    // Left
    i=0;
    for (i = y+1; i < y+h; i++) {
      PrintAt(x,i,"|");
    }
    // Right
    i=0;
    for (i = y+1; i < y+h; i++) {
      PrintAt(x+w,i,"|");
    }
  }
  /** Return the center-offset for a string of text
   */
  int centeroffset (int w, string text) {
    int offset;
    offset = (w-text.size())/2;
    return (offset);
  }

  /*
   * Move selection u/d by one. i points to index of current selection, n to new
   * (x,y) is UL corner of *Message Area*, w is width of same
   */
  int moveud (int last,vector<string> options,vector<string> prompt,int x, int y, int w,int prompth,int i, int n,int promptlines) {
    // are we still in region? else go to other end
    if ((i == 0) && (n < i)) {
      n = last;
    }
    if ((i == last) && (n>i)) {
      n=0;
    }
    string temp;
    temp=options[n];		// Scale to have spaces for background
    temp.resize(w,' ');
    InvertPrintAt(x,y+n,temp);	// Re-draw selected option
    temp=options[i];
    temp.resize(w,' ');
    PrintAt(x,y+i,temp);	// RE-draw old as unselected
    /* Display Prompt in prompt area*/
    string line;
    /* Clean Up message area */
    for (int j = 0; j < promptlines; j++) {
      SaveCursor();		// Cleanup and re-draw
      ResetCursor();
      SetCursor(1,y+prompth+j-1);
      EraseLine();
      RestoreCursor();
      PrintAt(x-1,y+prompth+j-1,"|");
      PrintAt(x+w+1,y+prompth+j-1,"|");
      temp=prompt[n];
      temp.resize(2*w,' ');	// To prevent array adress issues...
      line = string(temp.begin()+j*w,temp.begin()+(j+1)*w); // OKAY, shoULD FAIL IF STRING ELEMENT NOT THAT LONG. MAYBE USE ARRAY.
      PrintAt(x,y+prompth+j-1,line);
    }
    return(n);
  }

  /*
   * Draw a Horizontal (standard) menu at the locaation (x,y) of outer
   * box size w X h.  n is the number of options (menu will scroll if
   * required), options is a std::string array of entries, prompt is
   * short <2 lines at w-2 help for each option, which will be
   * displayed in bottom of menu
   * NOT FAILSAFE for daft entries, will happily draw impossible things.
   * As we are currently a function not a class not going to do
   * anything about this
   */
  int hmenu (int n, vector<string> options,vector<string> prompts,int x, int y, int w, int h,string title) {
    int promptlines=2;
    string temp;
    int prompth = h-promptlines-1;		// Prompt offset to 1st line text
    drawbox(x,y,w,h);
    w = w-2;
    h = h-2;			// Effective heights of textarea
    y = y+1;
    x = x+1;
    drawline(x,y+prompth-1,w); // Prompt Area
    int last=n-1;
    int i=0;
    string test;
    PrintAt(x+centeroffset(w,title),y,title); // This assumes title one line long
    y=y+1;			// or however many lines title
    // MUT ADD SCROLLING--either in terminal or on MBED by manually re-drawing


    for (i = 0; i < last-1; i++) { // Print all but last 2 lines
       temp=options[i];		// Scale to have spaces for background
       temp.resize(w,' ');
       PrintAt(x,y+i,temp);
    }
    n=last;
    i=moveud(last,options,prompts,x,y,w,prompth,i,n,promptlines); // Print last two lines

    while (1) {			// Let user navigate
      char c= Term->getc();
      switch (c) {
      case 'p':
	i=moveud(last,options,prompts,x,y,w,prompth,i,i-1,promptlines);
	continue;
      case 'n':
	i=moveud(last,options,prompts,x,y,w,prompth,i,i+1,promptlines);
	continue;
	/* Arrow Keys */
      case 0x1B:			// Escape sequence--this is long
	c = Term->getc();		// '[' char
	if (c == 0x5B) {
	  c = Term->getc();
	  switch(c) {		// which arrow, finally
	  case 0x41:		// Up
	    i=moveud(last,options,prompts,x,y,w,prompth,i,i-1,promptlines);
	    continue;
	  case 0x42:		// Down
	    i=moveud(last,options,prompts,x,y,w,prompth,i,i+1,promptlines);
	    continue;
	  case 0x43:		// Right (Ignore here)
	    continue;
	  case 0x44:		// Left (Ditto)
	    continue;
	  }
	}
      case '\r':		// Enter
	return(i);		// 'i' is index (0--n-1) of selection
      default:
	continue;
      }
    }
  }

  /*
   * Draw a config/checkbox) menu at the locaation (x,y) of outer
   * box size w X h.  n is the number of options (menu will scroll if
   * required), options is a std::string array of entries, prompt is
   * short <2 lines at w-2 help for each option, which will be
   * displayed in bottom of menu
   * NOT FAILSAFE for daft entries, will happily draw impossible things.
   * As we are currently a function not a class not going to do
   * anything about this. Return array of integers corresponding to selections
   */
  vector<int> chmenu (int n,vector<string> options,vector<string> prompts,int x, int y, int w, int h,string title,vector<int> defaults) {
    vector<int> s;		// Array of Selected Indexes
    vector<string> oldoptions=options;
    //    std::copy(options.begin(), options.end(), oldoptions.begin());
    int promptlines=2;
    int prompth = h-promptlines-1;		// Prompt offset to 1st line text
    drawbox(x,y,w,h);

    w = w-2;
    h = h-2;			// Effective heights of textarea
    y = y+1;
    x = x+1;
    drawline(x,y+prompth-1,w); // Prompt Area
    int last=n-1;
    int i=0;
    string test;
    PrintAt(x+centeroffset(w,title),y,title); // This assumes title one line long
    y=y+1;			// or however many lines title
    // MUT ADD SCROLLING--either in terminal or on MBED by manually re-drawing
    //    Term->sprintf(options[1]);
    for (i = 0; i < n; i++) {	// TODO: Add pre-seeding values in the return function
      std::stringstream ss;
      if(std::find(defaults.begin(), defaults.end(), i)!=defaults.end()){ // If set by default
	ss << "[X] " << options[i];
	s.push_back(i);
      } else {
	ss << "[ ] " << options[i];
      }
      options[i] = ss.str();
    }
    for (i = 0; i < last-1; i++) { // Print all but last 2 lines
      PrintAt(x,y+i,options[i]);
    }
    n=last;
    i=moveud(last,options,prompts,x,y,w,prompth,i,n,promptlines); // Print last two lines
    string t;
    while (1) {			// Let user navigate
      char c= Term->getc();
      switch (c) {
      case 'p':
	i=moveud(last,options,prompts,x,y,w,prompth,i,i-1,promptlines);
	continue;
      case 'n':
	i=moveud(last,options,prompts,x,y,w,prompth,i,i+1,promptlines);
	continue;
      case 0x1B:			// Escape sequence--this is long

	c = Term->getc();		// '[' char
	if (c == 0x5B) {
	  c = Term->getc();
	  switch(c) {		// which arrow, finally
	  case 0x41:		// Up
	    i=moveud(last,options,prompts,x,y,w,prompth,i,i-1,promptlines);
	    continue;
	  case 0x42:		// Down
	    i=moveud(last,options,prompts,x,y,w,prompth,i,i+1,promptlines);
	    continue;
	  case 0x43:		// Right (Ignore here)
	    continue;
	  case 0x44:		// Left (Ditto)
	    continue;
	  }
	}
      case ' ':
	if(std::find(s.begin(), s.end(), i)!=s.end()){
	  std::stringstream ss;
	  ss << "[ ] " << oldoptions[i];
	  options[i] = ss.str();
	  s.erase(remove(s.begin(), s.end(), i), s.end());
	} else {		// Toggle ON
	  std::stringstream ss;
	  ss << "[X] " << oldoptions[i];
	  options[i] = ss.str();
	  s.push_back(i);
	}
	if (i != 0) {
	  i=moveud(last,options,prompts,x,y,w,prompth,i-1,i,promptlines);
	} else {
	  i=moveud(last,options,prompts,x,y,w,prompth,i+1,i,promptlines);
	}
	continue;
      case '\r':		// Enter
	return(s);		// 'i' is index (0--n-1) of selection
      default:
	continue;
      }
    }
  }

  


  // UNSUPPORTED
  // /*
  //  * Bold PrintAt
  //  */
  // void BoldPrintAt(int x, int y, string s)
  // {
  //   SaveCursor();
  //   ResetCursor();
  //   SetCursor(x,y);
  //   Term->printf("\x1B[31m%s\x1B[39m",s.c_str());

  //   RestoreCursor();
  // }
  // /*
  //  * BoldInvert PrintAt
  //  */
  // void BoldInvertPrintAt(int x, int y, string s)
  // {
  //   SaveCursor();
  //   ResetCursor();
  //   SetCursor(x,y);
  //   Term->printf("\x1B[7m\x1B[1m%s\x1B[21m\x1B[27m",s.c_str());
  //   RestoreCursor();
  // }

  /** config menu with checkboxes // TODO: Implement as per paper.
   */
  // int cmenu {
  // }
private:
  Stream *Term;
};

#endif
