#include "mbed.h"
#include "stdio.h"
#include "math.h"
#include "cursing.h"
#include <iostream>
Cursing curse;			// The menu class.
Serial pc(USBTX, USBRX);
DigitalOut led1(LED1); // on-board LED1 
DigitalOut led2(LED2); // on-board LED2

int main() {
  pc.baud(460800);		// Needed as 9600 painfully slow
  wait(0.1);
  led1 = 1;
  curse.SetTerminal(&pc);
  curse.ResetCursor();
  curse.Clear();
  curse.PrintAt(4,10,"FooBar! (arbitrary string placement)");
  int x=0;
  int y=0;
  int screenw=80;
  int screenh=24;
  for (y=3; y <= 8; y++) {	// Print a box of scrrenwxcreenh at (x,y)
    for (x=40; x <= screenw; x++) {
      curse.SetCursor(x,y);
      pc.printf("#");
    }
  }
  pc.getc();	 // wait
  curse.drawbox(2,5,50,30);	// Draw some boxes to demonstate speed
  curse.drawbox(15,7,5,5);	// & overlapping
  curse.PrintAt(4,10,"this arbitrary text *DID* move the cursor");
  string message="this is the BR Corner";
  curse.PrintAt(screenw-message.size(),screenh,message);
  string message2="TR";
  curse.PrintAt(screenw-message2.size(),1,message2);
  pc.getc();			// Wait
  curse.Clear();
  string title="Title of Menu";
  vector<string> option={"Option 1","Option 2","A very long option!","another option"};
  int choice;
  vector<string> prompt={"Prompt1","Prompt 2","Another One","A Test of a far longer prompt example, which runs considerably over the limit limit limit limit limit limit limit limit limit limit limit limit limit limit limit limit limit limit limit limit limit limit"};
  choice=curse.hmenu(4,option,prompt,5,5,60,12,title); // Standard Menu--what to do
  curse.Clear();
  curse.ResetCursor();
  pc.printf("choice was: %s (now use case statements as default)",option[choice].c_str());
  pc.getc();			// Wait
  vector<int> config;
  curse.Clear();
  curse.ResetCursor();
  vector<string> options={"option 1","option 2","option 3","option 4"};
  vector<string> prompts={"a","b","c","d"};
  vector<int> defaults={1};
  config=curse.chmenu (4,options,prompts,5,5,60,12,title,defaults); // Config toggle menu
  for (int i: config) {
    switch (i) {
    case 0:
      pc.printf("zero");
      continue;
    case 1:
      pc.printf("one");
      continue;
    }
  }
  string r;
  getline(cin,r);

}
