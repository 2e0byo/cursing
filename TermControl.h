#ifndef TERMCONTROL_H
#define TERMCONTROL_H
#include "mbed.h"
#include <string>
//0x1B is escape
//#define FLASH_STRING const char * const

/** A simple class for sending VT100 terminal escape codes for handling things such as moving the cursor and clearing the screen
  */
class TermControl
{
    public:
    TermControl()
    {
    
    }
    /** @Param term The stream to send the escape codes to
     */
    TermControl(Stream* term)
    {
        Term=term;
    }
    void SetTerminal(Stream* term)
    {
        Term=term;
    }
    Stream* GetTerminal()
    {
        return Term;
    }
    //terminal control
    /** Resets the terminal to defaults
     */
    void Reset()
    {
        Print("\x1Bc");
    }
    /** Clears the screen
     */
    void Clear()
    {
        Term->printf("\x1B[2J");
    }
    /** Prints the specified string at coordinates x,y 
     *  The cursor position is not changed
     * 
     *  @param x The X coordinate, or column
     *  @param y The Y coordinate, or row
     *  @param s The string to print
     */
    void PrintAt(int x, int y, string s)
    {
        SaveCursor();
        ResetCursor();
        SetCursor(x,y);
        Print(s);
        RestoreCursor();
    }
    //todo void PrintfAt(int x, int y, string f, ...);
    /** Get a character from the terminal stream
     */
    char GetChar()
    {
        return Term->getc();
    }
    /** Sets the cursor's position to x,y
     */
    void SetCursor(int x, int y)
    {
        Term->printf("\x1B[%i;%iH",y,x);
    }
    /** Resets the cursor to (0,0), the top left hand corner of the screen
     */
    void ResetCursor()
    {
        Term->printf("\x1B[H");
    }
    /** Saves the cursor on the display-side. It can only remember one cursor position at a time.
     * ie, this can't be nested
     */
    void SaveCursor()
    {
        Term->printf("\x1B[s");
    }
    /** Restores the cursor on the display-side. It can only remember and store one cursor position at a time.
     * ie, this can't be nested
     */
    void RestoreCursor()
    {
        Term->printf("\x1B[u");
    }
    /** Enables a scrolling "window" at the specified beginning and ending rows
     * @param begin The beginning row
     * @param end The ending row
     */
    void EnableScrolling(int begin, int end) //begin and end are row numbers
    {
        Term->printf("\x1B[%i;%ir",begin,end);
    } 
    /** Scrolls down the scrolling "window" by 1 line. Note, EnableScrolling is required before this can be called
     */
    void ScrollDown()
    {
        Term->printf("\x1BD");
    }
    /** Scrolls up the scrolling "window" by 1 line. Note, EnableScrolling is required before this can be called
     */
    void ScrollUp()
    {
        Term->printf("\x1BM");
    }
    /** Erases the current line the cursor is on.
     */
    void EraseLine()
    {
        Term->printf("\x1B[2K");
    }
    /** Prints the specified string to the string at the current cursor position
     */
    void Print(string s)
    {
        Term->puts(s.c_str());
    }
    
    
    private:
    Stream *Term;
};
    



#endif