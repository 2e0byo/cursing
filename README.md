# README #

A very crude hack to port some of the ncurses functionality to the ARM
MBed, inspired by [TermControl](http://developer.mbed.org/users/earlz/code/TermControl/)

### Summary ###

Provides shortcuts to move cursor around and write at arbitrary
positions (from TermControl), and two standard menu types (select and
checkbox), documented below:



### Setting up ###
clone the repository or add `cursing.h` to your path. Then `#include "cursing.h`

### Contribution guidelines ###

Let me know, no mechanism in place atm.

### Documentation ###

There are three Main Functions, `drawbox`, `hmenu` and `chmenu`

#### Drawing Lines ####

```drawline(x,y,l)```
Draws horizontal line at (x,y) of length l. This
routine is used by the menu functions, and currently uses standard
ascii chars, not the VT100 line-drawing chars.

#### Drawing Boxes ####

Fairly simple, one simply passes the correct parameters to `drawbox`:

```drawbox(x,y,w,h);```

This routine is used by the menu functions,
and currently uses standard ascii chars, not the VT100 line-drawing
chars.

#### Standard Menu ####
```int hmenu (n,options[],prompts[],x, y, w, h,title)```

Draw standard menu with Upper LH corner at `(x,y)` and centered 1-line
title `title`, of `n` entries, and overall width `w` and height `h`.

`options` is a vector string array of options, `prompts` is 2-line
prompts to fit in the prompt area at the bottom of the menu,
corresponding to `options` (see `example.cpp`).

Arrow keys and `n` `p` navigate, enter selects. Returns an integer
corresponding to the address item selected, starting at 0.

#### Checkbox Menu ####
```  chmenu (n,options,prompts,x, y, w, h,title,defaults);```

Draw checkbox menu with Upper LH corner at `(x,y)` and centered 1-line
title `title`, of `n` entries, and overall width `w` and height `h`.

Arrow keys and `n` `p` navigate, `space` toggles checkbox, and enter
returns. Returns a vector of integers corresponding to the address items
selected, starting at 0. See `example.cpp`.

Defaults is a vector of integers corresponding to options to be
checked by default (e.g. `s` from previous run)
### Contact ###

email at `john dot morris at durham decimal ac point uk`


(replacing appropriate words)
